/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;


import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.PWMTalonSRX;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.PIDSubsystem;
import frc.robot.RobotMap;


/**
 * Add your docs here.
 */
public class ClimberPID extends PIDSubsystem {
  /**
   * Add your docs here.
   */
  
  private PWMTalonSRX climber_motor = new PWMTalonSRX(RobotMap.MOTEUR_CLIMBER_PWM);
  AnalogInput Pot2 = new AnalogInput(RobotMap.POT_CLIMBER_AI);



  public ClimberPID() {
    // Intert a subsystem name and PID values here
    super("ClimberPID", 10, 0, 0);
    setAbsoluteTolerance(0.05);
		getPIDController().setContinuous(false);
    
    // Use these to get going:
    // setSetpoint() - Sets where the PID controller should move the system
    // to
    

    //enable();// - Enables the PID controller.
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    //setDefaultCommand(new ClimbDefault());
  }

  @Override
  protected double returnPIDInput() {
    // Return your input value for the PID loop
    // e.g. a sensor, like a potentiometer:
    // yourPot.getAverageVoltage() / kYourMaxVoltage;
    return Pot2.getAverageVoltage();
  }

  @Override
  protected void usePIDOutput(double output) {
    // Use output to drive your system, like a motor
    // e.g. yourMotor.set(output);
   
    climber_motor.pidWrite(-output);
  }



  public void setClimber (double setpoint){
  
    if(setpoint == RobotMap.CLIMBER_CLIMBVALUE){
      Timer.delay(1);
      setSetpoint(setpoint);
    }
    else{
      setSetpoint(setpoint);
    }
    

}








}
