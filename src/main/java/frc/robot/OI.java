/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;


import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.commands.CancelAll;
import frc.robot.commands.CargoMecanism;
import frc.robot.commands.HatchGround;
import frc.robot.commands.HatchMecanism;
import frc.robot.commands.TankDriveInverse;


/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {

  public Joystick playerone  = new Joystick(0);
  public JoystickButton hatchmecanism;
  public JoystickButton cancelButton;
  public JoystickButton cargomecanism;
  public JoystickButton hatchground;
  //public JoystickButton climb;
  public JoystickButton preclimb;
  public JoystickButton compressorON;
  public JoystickButton compressorOFF;
  public JoystickButton driveinverseON;
  public JoystickButton driveinverseOFF;
  // public JoystickButton lowgear;
  // public JoystickButton highgear;

  
  
  
  
  public double GetJoystickRawAxis(int axis){
    return playerone.getRawAxis(axis);
  }

  OI(){

    hatchmecanism = new JoystickButton(playerone, RobotMap.B);
    hatchmecanism.whenPressed(new HatchMecanism());

    cargomecanism = new JoystickButton(playerone, RobotMap.A);
    cargomecanism.whenPressed(new CargoMecanism());

    hatchground = new JoystickButton(playerone, RobotMap.X);
    hatchground.whenPressed(new HatchGround());

    
    driveinverseON = new JoystickButton(playerone, RobotMap.LB);
    driveinverseON.whenPressed(new TankDriveInverse());
    
    driveinverseOFF = new JoystickButton(playerone, RobotMap.RB);
    
    cancelButton = new JoystickButton(playerone, RobotMap.Y);
    cancelButton.whenPressed(new CancelAll());
    // cancelButton.cancelWhenPressed(new CargoMecanism());
    // cancelButton.cancelWhenPressed(new HatchMecanism());
    // cancelButton.cancelWhenPressed(new HatchGround());
   

    //preclimb = new JoystickButton(playerone, RobotMap.Back);
    //preclimb.whenPressed(new ClimbSystem());

    //preclimb = new JoystickButton(playerone, RobotMap.RB);

    compressorON = new JoystickButton(playerone, RobotMap.Start);
    compressorOFF = new JoystickButton(playerone, RobotMap.Back);

  }

  //// CREATING BUTTONS
  // One type of button is a joystick button which is any button on a
  //// joystick.
  // You create one by telling it which joystick it's on and which button
  // number it is.
  // Joystick stick = new Joystick(port);
  // Button button = new JoystickButton(stick, buttonNumber);

  // There are a few additional built in buttons you can use. Additionally,
  // by subclassing Button you can create custom triggers and bind those to
  // commands the same as any other Button.

  //// TRIGGERING COMMANDS WITH BUTTONS
  // Once you have a button, it's trivial to bind it to a button in one of
  // three ways:

  // Start the command when the button is pressed and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenPressed(new ExampleCommand());

  // Run the command while the button is being held down and interrupt it once
  // the button is released.
  // button.whileHeld(new ExampleCommand());

  // Start the command when the button is released and let it run the command
  // until it is finished as determined by it's isFinished method.
  // button.whenReleased(new ExampleCommand());
}
