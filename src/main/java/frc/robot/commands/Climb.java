/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;


import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class Climb extends Command {
  public Climb() {
    // Use requires() here to declare subsystem dependencies
  requires(Robot.climbSystem);
  requires(Robot.elevator);
  requires(Robot.cargoIntake);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.climbSystem.setPistonSwag(true);
    Robot.climbSystem.setPistonBlock(true);
    Robot.climber.setClimber(RobotMap.CLIMBER_CLIMBVALUE);
    Robot.elevator.setElevator(RobotMap.AJOUTCLIMBSETPOINT);
    Robot.cargoIntake.setSpeed(0, 2);
    
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return Robot.m_oi.preclimb.get();
   
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
