/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class TankDriveInverse extends Command {
  public TankDriveInverse() {
    // Use requires() here to declare subsystem dependencies
    requires(Robot.driveTrain);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double moveSpeed = 0;
    if(Robot.m_oi.playerone.getRawAxis(3) > 0){
      moveSpeed = Robot.m_oi.playerone.getRawAxis(3);
    }
    else{
      moveSpeed = -Robot.m_oi.playerone.getRawAxis(2);

    }
    
    
		double rotateSpeed = Robot.m_oi.playerone.getRawAxis(0);

    Robot.driveTrain.arcadeDrive(-moveSpeed, rotateSpeed);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return Robot.m_oi.driveinverseOFF.get();
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
