/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;


import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.commands.ClimbDefault;

/**
 * Add your docs here.
 */
public class ClimberSystem extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  DoubleSolenoid piston_swag = new DoubleSolenoid(1,1, 6);
  Solenoid piston_blockNballshifterelevetor = new Solenoid(0,4);
  //AnalogInput Pot2 = new AnalogInput(RobotMap.POT_ELEVATEUR_AI);
  

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new ClimbDefault());
  }

  public void setPistonSwag (boolean state){
   
    if(state == true){
  
      piston_swag.set(DoubleSolenoid.Value.kReverse);
  
     }
     if(state == false){
    
      piston_swag.set(DoubleSolenoid.Value.kForward);
  
     }
   
  
  }
  
  
  public void setPistonBlock(boolean state){
  
  if(state == false){
  
  piston_blockNballshifterelevetor.set(false);
  }
  if(state == true){
  
  piston_blockNballshifterelevetor.set(true);
  
  }
  }

}
